import nuke


def autoFormat():
    formatVersion = 0
    # MAKE A LIST OUT OF ALL AVAILABLE FORMATS
    f = nuke.formats()
    fList = map(lambda x: x.name(), f)
    node = nuke.selectedNode()
    
    if node == None or node.Class() != "Read" :
        nuke.message("Please select a Read node")
    nodeFormat = node.format()
    # CREATE A NEW FORMAT BASED ON THE READ1 FORMAT AND SET IT A S DEFAULT VALUE FOR THE PROJECT
    readFormat = str(nodeFormat.width()) + " " + str(nodeFormat.height()) + "readFormat"

    if "readFormat" in fList:
        formatVersion += 1
        nuke.addFormat(readFormat + str(formatVersion))
        root = nuke.root()
        root.knob('format').setValue("readFormat" + str(formatVersion))
    else:
        nuke.addFormat(readFormat)
        root = nuke.root()
        root.knob('format').setValue("readFormat")       

