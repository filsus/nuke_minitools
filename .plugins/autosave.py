import nuke
import os

class readNameParser:
    def __init__(self, readPath):      
        self.readPath = readPath

    def readName(self):
        pathSplit = self.readPath.split('/')[-1]
        readName = pathSplit.split('.')[0]
        return readName

    def fileDirec(self):
        pathSplit = self.readPath.split('/')
        if "plate" or "plates" in pathSplit:
            pathArray = pathSplit[:-2]
            fileDirec = '/'.join(pathArray)
            return str(fileDirec)
        else:
            pathArray = pathSplit[:-1]
            fileDirec = '/'.join(pathArray)
            return str(fileDirec)

class autoSave:    
    def autoSave(self):
        selNode = nuke.selectedNode()
        if selNode == None or selNode.Class() != "Read" :
            nuke.message("Please select a Read node")
        readPath = selNode.knob("file").value()
        path = readNameParser(readPath)
        scriptPath = path.fileDirec() + "/scripts/" + path.readName() + ".nk"
                    
        answer = nuke.ask("Do you want to save in: " + str(scriptPath) + "?")
        if answer:
            newDirectory = path.fileDirec() + "/scripts"

            if not os.path.exists(newDirectory):
                os.makedirs(newDirectory)
            
            root = nuke.root()
            root.knob('name').setValue(scriptPath)
            nuke.scriptSave()   