import nuke

class Alligners:
    def verAlligner(self):
        nodes = nuke.selectedNodes()
        nodeList = map(lambda x: x['ypos'].value(), nodes)
        smallest = min(nodeList)
        nodeIndex = nodeList.index(smallest)
        topNode = nodes[nodeIndex]
        topnodeWidth = topNode.screenWidth()
        topNodeXPos = topNode['xpos'].value()

        for item in nodes:
            itemWidth = item.screenWidth()
            item['xpos'].setValue( topNodeXPos+ topnodeWidth/2 - itemWidth/2 )

    def horAlligner(self):
        nodes = nuke.selectedNodes()
        nodeList = map(lambda x: x['xpos'].value(), nodes)
        smallest = min(nodeList)
        nodeIndex = nodeList.index(smallest)
        topNode = nodes[nodeIndex]
        topnodeHeight = topNode.screenHeight()
        topNodeYPos = topNode['ypos'].value()

        for item in nodes:
            itemHeight = item.screenHeight()
            item['ypos'].setValue( topNodeYPos+ topnodeHeight/2 - itemHeight/2 )


