![compatibility](https://img.shields.io/badge/compatibility-Nuke%2FNukeX%2011%2C12-green)

***NUKE MINI TOOLS***
*********************************************************************
Last modified: 18/01/2020
*********************************************************************
Version 1.0
*********************************************************************
Compatibility Nuke/NukeX 12,12
*********************************************************************
***HOW TO INSTALL AND RUN:***
**********************************************************************************************************************
1. Navigate to you /.nuke directory (this should be automatically created after Nuke startup)

2. If the original menu.py and init.py files are not empty copy the code from them, otherwise replace the folders

3. Copy the /.plugins folder to you /.nuke directory

4. Open Nuke

5. A new icon will appear in the nuke Toolbar with each tool and assigned shortcut
***********************************************************************************************************************
***Alligner Tool***

Select bunch of nodes and press "a" to allign vertically or "alt+a" to allign horizontally
***********************************************************************************************************************
***Autoformat Tool***

Select a Read node and press "ctrl+F" to match the Project format with the selected Read node format
***********************************************************************************************************************
***Autosave Tool***

Select a Read node and press "u"to fast save a sript with the selected Read nodes name in the Project/scripts directory. The 
Tool will create a /scripts folder in the Read nodes subdirectory if it is situated in the Project/"plate" or "plates" folder.
***********************************************************************************************************************
***Scalenodes Tool***

Select nodes within the Node graph and press "n" to scale the distance between nodes within a selected group.

ⓒ  [Copyright](https://learn.foundry.com/nuke/developers/63/pythondevguide/dag.html)
***********************************************************************************************************************
CREATED BY:
****************************
suska.filip@gmail.com
lukas.danko1@gmail.com


