import nuke
import nukescripts
import alligner
import autoformat
import autosave
import scalenodes

nukemenu = nuke.menu( 'Nodes')
plugins = nukemenu.addMenu("Plugins", icon= "pluginicon.png")
nukemenu.addCommand('Plugins/scaleNodes', "scalenodes.scaleNodes(2)", "n")
alligner = alligner.Alligners()
nukemenu.addCommand("Plugins/verAlligner", "alligner.verAlligner()","a")
nukemenu.addCommand("Plugins/horAlligner", "alligner.horAlligner()","alt+a")
autosave =autosave.autoSave()
nukemenu.addCommand("Plugins/autoSave", "autosave.autoSave()","u")
nukemenu.addCommand("Plugins/autoFormat", "autoformat.autoFormat()","ctrl+f")